# Movie Collection

Want to collect movies & save to your phone? Movie Collection is the answers!

# Well-to-known

## Tech Stack & Architecture

### State Management

This project is using MobX as a state management ([MobX](https://pub.dev/packages/mobx)).

MobX is a state-management library that makes it simple to connect the reactive data of 
your application with the UI. This wiring is completely automatic and feels very natural.
As the application-developer, you focus purely on what reactive-data needs to be consumed 
in the UI (and elsewhere) without worrying about keeping the two in sync.

![Mechanism](https://mobx.netlify.app/assets/images/mobx-triad-1024e4f4e0ff0ce3e27a2da8e36ab3a4.png)

At the heart of MobX are three important concepts: Observables, Actions and Reactions.

### Clean Architecture

We separate *data*, *domain*, and *presentation* layer so that development and testing
progress is more effective.

![Scheme](https://i0.wp.com/resocoder.com/wp-content/uploads/2019/08/Clean-Architecture-Flutter-Diagram.png?w=556&ssl=1)
