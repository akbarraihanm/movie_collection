import 'package:movie_collection/di/app_locator.dart';
import 'package:movie_collection/domain/movies/data/data_source/movie_data_source.dart';
import 'package:movie_collection/domain/movies/data/repository/movie_repository_impl.dart';
import 'package:movie_collection/domain/movies/data/service/movie_service.dart';
import 'package:movie_collection/domain/movies/domain/repository/movie_repository.dart';
import 'package:movie_collection/domain/movies/domain/use_cases/add_movie_use_case.dart';
import 'package:movie_collection/domain/movies/domain/use_cases/delete_movie_use_case.dart';
import 'package:movie_collection/domain/movies/domain/use_cases/get_movie_use_case.dart';
import 'package:movie_collection/domain/movies/domain/use_cases/update_movie_use_case.dart';

class MovieDomainLocator {
  MovieDomainLocator() {
    sl.registerLazySingleton(() => MovieService());
    sl.registerLazySingleton<MovieDataSource>(() => MovieDataSourceImpl(sl()));
    sl.registerLazySingleton<MovieRepository>(() => MovieRepositoryImpl(sl()));
    sl.registerLazySingleton(() => GetMovieUseCase(sl()));
    sl.registerLazySingleton(() => AddMovieUseCase(sl()));
    sl.registerLazySingleton(() => UpdateMovieUseCase(sl()));
    sl.registerLazySingleton(() => DeleteMovieUseCase(sl()));
  }
}