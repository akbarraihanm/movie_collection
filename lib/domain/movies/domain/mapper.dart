import 'package:movie_collection/domain/movies/data/models/movie_data.dart';
import 'package:movie_collection/domain/movies/domain/entity/movie_entity.dart';

extension MovieDataToEntity on MovieData {
  MovieEntity toEntity() => MovieEntity(
    id: id ?? "",
    title: title ?? "",
    director: director ?? "",
    summary: summary ?? "",
    genres: genres ?? [],
  );
}

extension MovieEntityToData on MovieEntity {
  MovieData toData() => MovieData(
    id: id,
    title: title,
    director: director,
    summary: summary,
    genres: genres,
  );
}