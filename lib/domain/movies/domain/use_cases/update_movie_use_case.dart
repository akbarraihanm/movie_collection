import 'package:movie_collection/core/util/use_case.dart';
import 'package:movie_collection/domain/movies/domain/entity/movie_entity.dart';
import 'package:movie_collection/domain/movies/domain/repository/movie_repository.dart';

class UpdateMovieUseCase extends UseCase<int, MovieEntity> {
  final MovieRepository repository;

  UpdateMovieUseCase(this.repository);

  @override
  int call(MovieEntity param) {
    return repository.updateMovie(param);
  }
}