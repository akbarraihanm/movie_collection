import 'package:movie_collection/core/util/use_case.dart';
import 'package:movie_collection/domain/movies/domain/entity/movie_entity.dart';
import 'package:movie_collection/domain/movies/domain/repository/movie_repository.dart';

class GetMovieUseCase extends UseCase<List<MovieEntity>, NoParams> {
  final MovieRepository repository;

  GetMovieUseCase(this.repository);

  @override
  List<MovieEntity> call(NoParams param) {
    return repository.getMovies();
  }

}