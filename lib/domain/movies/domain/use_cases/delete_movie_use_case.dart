import 'package:movie_collection/core/util/use_case.dart';
import 'package:movie_collection/domain/movies/domain/repository/movie_repository.dart';

class DeleteMovieUseCase extends UseCase<int, String> {
  final MovieRepository repository;

  DeleteMovieUseCase(this.repository);

  @override
  int call(String param) {
    return repository.deleteMovie(param);
  }
}