import 'package:movie_collection/core/util/use_case.dart';
import 'package:movie_collection/domain/movies/data/models/movie_data.dart';
import 'package:movie_collection/domain/movies/domain/repository/movie_repository.dart';

class AddMovieUseCase extends UseCase<int, MovieData> {
  final MovieRepository repository;

  AddMovieUseCase(this.repository);

  @override
  int call(MovieData param) {
    return repository.addMovie(param);
  }

}