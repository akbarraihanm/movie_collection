import 'package:movie_collection/domain/movies/data/models/movie_data.dart';
import 'package:movie_collection/domain/movies/domain/entity/movie_entity.dart';

abstract class MovieRepository {
  List<MovieEntity> getMovies();
  int addMovie(MovieData data);
  int updateMovie(MovieEntity data);
  int deleteMovie(String id);
}