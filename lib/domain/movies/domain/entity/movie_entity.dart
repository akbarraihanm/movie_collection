class MovieEntity {
  final String id;
  final String title;
  final String director;
  final String summary;
  final List<String> genres;

  MovieEntity({
    required this.id,
    required this.title,
    required this.director,
    required this.summary,
    required this.genres,
  });
}