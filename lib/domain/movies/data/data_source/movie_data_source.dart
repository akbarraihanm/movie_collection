import 'package:movie_collection/domain/movies/data/models/movie_data.dart';
import 'package:movie_collection/domain/movies/data/service/movie_service.dart';

abstract class MovieDataSource {
  List<MovieData> getMovies();
  int addMovie(MovieData data);
  int updateMovie(MovieData data);
  int deleteMovie(String id);
}

class MovieDataSourceImpl implements MovieDataSource {
  final MovieService service;

  MovieDataSourceImpl(this.service);

  @override
  int addMovie(MovieData data) {
    return service.addMovie(data);
  }

  @override
  int deleteMovie(String id) {
    return service.deleteMovie(id);
  }

  @override
  List<MovieData> getMovies() {
    return service.getMovies();
  }

  @override
  int updateMovie(MovieData data) {
    return service.updateMovie(data);
  }

}