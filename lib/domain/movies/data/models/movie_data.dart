class MovieData {
  String? id;
  String? title;
  String? director;
  String? summary;
  List<String>? genres;

  MovieData({
    this.id,
    this.title,
    this.director,
    this.summary,
    this.genres,
  });

  Map<String, dynamic> toJson() => {
    'id': id,
    'title': title,
    'director': director,
    'summary': summary,
    'genres': genres,
  };
}