import 'package:movie_collection/domain/movies/data/data_source/movie_data_source.dart';
import 'package:movie_collection/domain/movies/data/models/movie_data.dart';
import 'package:movie_collection/domain/movies/domain/entity/movie_entity.dart';
import 'package:movie_collection/domain/movies/domain/mapper.dart';
import 'package:movie_collection/domain/movies/domain/repository/movie_repository.dart';

class MovieRepositoryImpl implements MovieRepository {
  final MovieDataSource dataSource;

  MovieRepositoryImpl(this.dataSource);

  @override
  int addMovie(MovieData data) {
    return dataSource.addMovie(data);
  }

  @override
  int deleteMovie(String id) {
    return dataSource.deleteMovie(id);
  }

  @override
  List<MovieEntity> getMovies() {
    return dataSource.getMovies().map((e) => e.toEntity()).toList();
  }

  @override
  int updateMovie(MovieEntity data) {
    return dataSource.updateMovie(data.toData());
  }

}