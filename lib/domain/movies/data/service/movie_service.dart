import 'package:movie_collection/core/const/app_const.dart';
import 'package:movie_collection/domain/movies/data/models/movie_data.dart';

class MovieService {
  List<MovieData> getMovies() {
    return AppConst.movies;
  }

  int addMovie(MovieData data) {
    try {
      AppConst.movies.add(data);
      return 1;
    } catch (ex) {
      return 0;
    }
  }

  int updateMovie(MovieData data) {
    try {
      int index = 0;
      AppConst.movies.asMap().forEach((key, value) {
        if (value.id == data.id) index = key;
      });
      AppConst.movies[index] = data;
      return 1;
    } catch (ex) {
      return 0;
    }
  }

  int deleteMovie(String id) {
    try {
      AppConst.movies.removeWhere((movie) => movie.id == id);
      return 1;
    } catch (ex) {
      return 0;
    }
  }
}