import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:movie_collection/core/route/app_route.gr.dart';

@RoutePage()
class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    _goToMovieList();
  }

  void _goToMovieList() {
    Future.delayed(const Duration(seconds: 2), () {
      context.router.replace(MovieListRoute());
    });
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Colors.lightGreenAccent,
      body: Center(
        child: Icon(
          Icons.movie,
          size: 72,
        ),
      ),
    );
  }
}
