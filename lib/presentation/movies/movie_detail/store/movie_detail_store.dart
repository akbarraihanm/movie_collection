import 'package:mobx/mobx.dart';
import 'package:movie_collection/core/util/use_case.dart';
import 'package:movie_collection/domain/movies/data/models/movie_data.dart';
import 'package:movie_collection/domain/movies/domain/entity/movie_entity.dart';
import 'package:movie_collection/domain/movies/domain/mapper.dart';
import 'package:movie_collection/domain/movies/domain/use_cases/add_movie_use_case.dart';
import 'package:movie_collection/domain/movies/domain/use_cases/delete_movie_use_case.dart';
import 'package:movie_collection/domain/movies/domain/use_cases/get_movie_use_case.dart';
import 'package:movie_collection/domain/movies/domain/use_cases/update_movie_use_case.dart';
import 'package:uuid/uuid.dart';

part 'movie_detail_store.g.dart';

class MovieDetailStore = MovieDetailBase with _$MovieDetailStore;

abstract class MovieDetailBase with Store {
  final UpdateMovieUseCase updateMovieUseCase;
  final DeleteMovieUseCase deleteMovieUseCase;
  final AddMovieUseCase addMovieUseCase;
  final GetMovieUseCase getMovieUseCase;

  MovieDetailBase({
    required this.updateMovieUseCase,
    required this.deleteMovieUseCase,
    required this.addMovieUseCase,
    required this.getMovieUseCase,
  });

  @observable
  var selected = ObservableList<String>.of([]);

  MovieEntity data = MovieData().toEntity();

  @observable
  int? result;

  @observable
  int? deleteResult;

  @observable
  bool isValid = false;

  @action
  void checkIfDetail(String id) {
    if (id.isNotEmpty) {
      for (var e in getMovieUseCase(NoParams())) {
        if (e.id == id) data = e;
      }
    }
  }

  @action
  void addMovie({
    required String title,
    required String director,
    required String summary,
  }) {
    final movieData = MovieData(
      id: const Uuid().v4(),
      title: title,
      summary: summary,
      genres: selected,
      director: director,
    );
    result = addMovieUseCase(movieData);
  }

  @action
  void validate({
    required String title,
    required String director,
    required String summary,
  }) {
    if (title.isNotEmpty && director.isNotEmpty && summary.isNotEmpty
        && selected.isNotEmpty) {
      isValid = true;
    } else {
      isValid = false;
    }
  }

  @action
  void addGenre(String item) {
    selected.add(item);
  }

  @action
  void removeGenre(String item) {
    selected.remove(item);
  }

  @action
  void updateMovie({
    required String title,
    required String director,
    required String summary,
  }) {
    final movieData = MovieEntity(
      id: data.id,
      title: title,
      summary: summary,
      genres: selected,
      director: director,
    );
    result = updateMovieUseCase(movieData);
  }

  @action
  int deleteMovie() {
    return deleteResult = deleteMovieUseCase(data.id);
  }
}