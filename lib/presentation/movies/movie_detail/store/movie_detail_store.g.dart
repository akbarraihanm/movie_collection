// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_detail_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$MovieDetailStore on MovieDetailBase, Store {
  late final _$selectedAtom =
      Atom(name: 'MovieDetailBase.selected', context: context);

  @override
  ObservableList<String> get selected {
    _$selectedAtom.reportRead();
    return super.selected;
  }

  @override
  set selected(ObservableList<String> value) {
    _$selectedAtom.reportWrite(value, super.selected, () {
      super.selected = value;
    });
  }

  late final _$resultAtom =
      Atom(name: 'MovieDetailBase.result', context: context);

  @override
  int? get result {
    _$resultAtom.reportRead();
    return super.result;
  }

  @override
  set result(int? value) {
    _$resultAtom.reportWrite(value, super.result, () {
      super.result = value;
    });
  }

  late final _$deleteResultAtom =
      Atom(name: 'MovieDetailBase.deleteResult', context: context);

  @override
  int? get deleteResult {
    _$deleteResultAtom.reportRead();
    return super.deleteResult;
  }

  @override
  set deleteResult(int? value) {
    _$deleteResultAtom.reportWrite(value, super.deleteResult, () {
      super.deleteResult = value;
    });
  }

  late final _$isValidAtom =
      Atom(name: 'MovieDetailBase.isValid', context: context);

  @override
  bool get isValid {
    _$isValidAtom.reportRead();
    return super.isValid;
  }

  @override
  set isValid(bool value) {
    _$isValidAtom.reportWrite(value, super.isValid, () {
      super.isValid = value;
    });
  }

  late final _$MovieDetailBaseActionController =
      ActionController(name: 'MovieDetailBase', context: context);

  @override
  void checkIfDetail(String id) {
    final _$actionInfo = _$MovieDetailBaseActionController.startAction(
        name: 'MovieDetailBase.checkIfDetail');
    try {
      return super.checkIfDetail(id);
    } finally {
      _$MovieDetailBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addMovie(
      {required String title,
      required String director,
      required String summary}) {
    final _$actionInfo = _$MovieDetailBaseActionController.startAction(
        name: 'MovieDetailBase.addMovie');
    try {
      return super.addMovie(title: title, director: director, summary: summary);
    } finally {
      _$MovieDetailBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void validate(
      {required String title,
      required String director,
      required String summary}) {
    final _$actionInfo = _$MovieDetailBaseActionController.startAction(
        name: 'MovieDetailBase.validate');
    try {
      return super.validate(title: title, director: director, summary: summary);
    } finally {
      _$MovieDetailBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addGenre(String item) {
    final _$actionInfo = _$MovieDetailBaseActionController.startAction(
        name: 'MovieDetailBase.addGenre');
    try {
      return super.addGenre(item);
    } finally {
      _$MovieDetailBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void removeGenre(String item) {
    final _$actionInfo = _$MovieDetailBaseActionController.startAction(
        name: 'MovieDetailBase.removeGenre');
    try {
      return super.removeGenre(item);
    } finally {
      _$MovieDetailBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void updateMovie(
      {required String title,
      required String director,
      required String summary}) {
    final _$actionInfo = _$MovieDetailBaseActionController.startAction(
        name: 'MovieDetailBase.updateMovie');
    try {
      return super
          .updateMovie(title: title, director: director, summary: summary);
    } finally {
      _$MovieDetailBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  int deleteMovie() {
    final _$actionInfo = _$MovieDetailBaseActionController.startAction(
        name: 'MovieDetailBase.deleteMovie');
    try {
      return super.deleteMovie();
    } finally {
      _$MovieDetailBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
selected: ${selected},
result: ${result},
deleteResult: ${deleteResult},
isValid: ${isValid}
    ''';
  }
}
