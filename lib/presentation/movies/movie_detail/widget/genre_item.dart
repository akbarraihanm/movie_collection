import 'package:flutter/material.dart';
import 'package:movie_collection/core/config/app_typography.dart';

class GenreItem extends StatelessWidget {
  final String item;
  final double? fontSize;
  final List<String> selected;
  final bool isCreate;
  final Function onTap;

  const GenreItem({
    Key? key,
    required this.item,
    this.fontSize,
    required this.selected,
    this.isCreate = false,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () {
        if (isCreate) onTap.call();
      },
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(2.5),
          border: Border.all(color: Colors.blue),
          color: selected.contains(item)? Colors.green: null,
        ),
        child: Text(
          item,
          style: AppTypography.copyWith(
            size: fontSize ?? 12,
            color: selected.contains(item)? Colors.white: null,
          ),
        ),
      ),
    );
  }
}
