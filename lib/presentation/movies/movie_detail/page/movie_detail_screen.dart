import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';
import 'package:movie_collection/core/const/app_const.dart';
import 'package:movie_collection/core/widgets/app_bar/app_bar.dart';
import 'package:movie_collection/core/widgets/button/app_button.dart';
import 'package:movie_collection/core/widgets/forms/app_form_field.dart';
import 'package:movie_collection/core/widgets/forms/form_column_widget.dart';
import 'package:movie_collection/di/app_locator.dart';
import 'package:movie_collection/presentation/movies/movie_detail/store/movie_detail_store.dart';
import 'package:movie_collection/presentation/movies/movie_detail/widget/genre_item.dart';

@RoutePage()
class MovieDetailScreen extends StatefulWidget {
  final String id;
  
  const MovieDetailScreen({
    Key? key, 
    @PathParam('id') required this.id,
  }) : super(key: key);

  @override
  State<MovieDetailScreen> createState() => _MovieDetailScreenState();
}

class _MovieDetailScreenState extends State<MovieDetailScreen> {
  final _titleCtrl = TextEditingController();
  final _directorCtrl = TextEditingController();
  final _summaryCtrl = TextEditingController();

  final _summaryNode = FocusNode();
  
  final _store = MovieDetailStore(
    updateMovieUseCase: sl(), 
    deleteMovieUseCase: sl(),
    addMovieUseCase: sl(),
    getMovieUseCase: sl(),
  );
  
  @override
  void initState() {
    super.initState();
    _store.checkIfDetail(widget.id);
    if (widget.id.isNotEmpty) {
      _setData();
    }
  }

  void _setData() {
    final data = _store.data;
    _titleCtrl.text = data.title;
    _directorCtrl.text = data.director;
    _summaryCtrl.text = data.summary;
    _store.selected = ObservableList<String>.of(data.genres);
    _store.validate(
      title: _titleCtrl.text,
      director: _directorCtrl.text,
      summary: _summaryCtrl.text,
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar.backBtn(
        title: _store.data.title.isEmpty?
        'Create Movie': _store.data.title,
        actions: [
          Visibility(
            visible: widget.id.isNotEmpty,
            child: InkWell(
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              onTap: () {
                if (_store.deleteMovie() == 1) {
                  context.router.pop(['delete']);
                }
              },
              child: const Icon(
                Icons.delete,
                size: 24,
                color: Colors.red,
              ),
            ),
          ),
          const SizedBox(width: 16),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              padding: const EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  FormColumnWidget(
                    title: "Title",
                    child: AppFormField(
                      hint: "Input Title",
                      borderColor: Colors.blue,
                      controller: _titleCtrl,
                      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                      onChanged: (v) => _store.validate(
                        title: _titleCtrl.text,
                        summary: _summaryCtrl.text,
                        director: _directorCtrl.text,
                      ),
                    ),
                  ),
                  FormColumnWidget(
                    title: "Director",
                    child: AppFormField(
                      hint: "Input Director",
                      borderColor: Colors.blue,
                      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                      controller: _directorCtrl,
                      onChanged: (v) => _store.validate(
                        title: _titleCtrl.text,
                        summary: _summaryCtrl.text,
                        director: _directorCtrl.text,
                      ),
                    ),
                  ),
                  FormColumnWidget(
                    title: "Summary",
                    child: AppFormField(
                      hint: "Input Summary",
                      height: 200,
                      focusNode: _summaryNode,
                      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                      borderColor: Colors.blue,
                      inputType: TextInputType.multiline,
                      isMultiline: true,
                      controller: _summaryCtrl,
                      onChanged: (v) => _store.validate(
                        title: _titleCtrl.text,
                        summary: _summaryCtrl.text,
                        director: _directorCtrl.text,
                      ),
                    ),
                  ),
                  FormColumnWidget(
                    title: "Select Genre(s)",
                    child: Wrap(
                      direction: Axis.horizontal,
                      spacing: 12,
                      runSpacing: 12,
                      children: AppConst.movieGenres.map((e) {
                        return Observer(
                          builder: (_) {
                            return GenreItem(
                              item: e,
                              selected: _store.selected,
                              fontSize: 14,
                              isCreate: true,
                              onTap: () {
                                setState(() {
                                  if (_store.selected.contains(e)) {
                                    _store.removeGenre(e);
                                  } else {
                                    _store.addGenre(e);
                                  }
                                });
                                _store.validate(
                                  title: _titleCtrl.text,
                                  director: _directorCtrl.text,
                                  summary: _summaryCtrl.text,
                                );
                              },
                            );
                          }
                        );
                      }).toList(),
                    ),
                  )
                ],
              ),
            ),
          ),
          Observer(
            builder: (context) {
              if (_store.result == 1) {
                if (widget.id.isNotEmpty) {
                  context.router.pop(['update']);
                } else {
                  final title = _titleCtrl.text;
                  context.router.pop(['create', title]);
                }
              }
              return AppButton(
                title: widget.id.isNotEmpty? "Update Movie": "Create Movie",
                margin: const EdgeInsets.all(16),
                isEnable: _store.isValid,
                onPressed: () {
                  if (widget.id.isNotEmpty) {
                    _store.updateMovie(
                      title: _titleCtrl.text,
                      summary: _summaryCtrl.text,
                      director: _directorCtrl.text,
                    );
                  } else {
                    _store.addMovie(
                      title: _titleCtrl.text,
                      summary: _summaryCtrl.text,
                      director: _directorCtrl.text,
                    );
                  }
                },
              );
            }
          )
        ],
      ),
    );
  }
}

