import 'package:auto_route/auto_route.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:movie_collection/core/config/app_typography.dart';
import 'package:movie_collection/core/const/app_const.dart';
import 'package:movie_collection/core/route/app_route.gr.dart';
import 'package:movie_collection/core/widgets/app_bar/app_bar.dart';
import 'package:movie_collection/core/widgets/forms/app_form_field.dart';
import 'package:movie_collection/di/app_locator.dart';
import 'package:movie_collection/presentation/movies/movie_list/store/movie_list_store.dart';
import 'package:movie_collection/presentation/movies/movie_list/widget/movie_item.dart';

@RoutePage()
class MovieListScreen extends StatelessWidget {
  MovieListScreen({Key? key}) : super(key: key);

  final _store = MovieListStore(sl());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar.titled(title: "Movie Collection"),
      body: Column(
        children: [
          AppFormField(
            margin: const EdgeInsets.fromLTRB(16, 16, 16, 0),
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
            hint: "Search by title...",
            bgColor: Colors.white,
            borderColor: Colors.green,
            onChanged: (v) {
              _store.searchMovies(v);
              if (v.isEmpty) _store.getMovies();
            },
          ),
          Expanded(
            child: Observer(
              builder: (_) {
                final list = _store.list;
                if (list.isEmpty) {
                  return Center(
                    child: Text(
                      AppConst.noMovies,
                      textAlign: TextAlign.center,
                      style: AppTypography.copyWith(color: Colors.grey),
                    ),
                  );
                }
                return ListView.separated(
                  padding: const EdgeInsets.all(16),
                  itemCount: list.length,
                  shrinkWrap: true,
                  itemBuilder: (context, position) => MovieItem(
                    data: list[position],
                    onTap: () async {
                      try {
                        dynamic result = await context.router.push(MovieDetailRoute(
                          id: list[position].id,
                        ));

                        if (result != null) {

                          String message = "";
                          /// Trigger after delete success
                          if (context.mounted && result[0] == 'delete') {
                            message = 'Movie ${list[position].title} has been deleted';
                          }

                          /// Trigger after update success
                          if (context.mounted && result[0] == 'update') {
                            message = 'Movie has been updated';
                          }

                          if (context.mounted && message.isNotEmpty) {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text(
                                message,
                                style: AppTypography.copyWith(color: Colors.white),
                              ),
                            ));
                          }
                        }

                        _store.getMovies();
                      } catch (ex) {
                        if (kDebugMode) {
                          print("Error is $ex");
                        }
                      }
                    },
                  ),
                  separatorBuilder: (_,__) => const SizedBox(height: 8),
                );
              }
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          dynamic result = await context.router.push(MovieDetailRoute(id: ''));

          if (result != null) {
            /// Trigger after create success
            if (context.mounted && result[0] == 'create') {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text(
                  'Movie ${result[1]} has been created',
                  style: AppTypography.copyWith(color: Colors.white),
                ),
              ));
            }
          }

          _store.getMovies();
        },
        backgroundColor: Colors.green,
        child: const Icon(Icons.add, size: 24, color: Colors.white),
      ),
    );
  }
}
