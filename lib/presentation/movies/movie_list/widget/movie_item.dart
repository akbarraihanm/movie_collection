import 'package:flutter/material.dart';
import 'package:movie_collection/core/config/app_typography.dart';
import 'package:movie_collection/domain/movies/domain/entity/movie_entity.dart';
import 'package:movie_collection/presentation/movies/movie_detail/widget/genre_item.dart';

class MovieItem extends StatelessWidget {
  final MovieEntity data;
  final Function onTap;

  const MovieItem({
    Key? key,
    required this.data,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      onTap: () => onTap.call(),
      child: Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          boxShadow: const [BoxShadow(
            color: Colors.black12,
            blurRadius: 4,
            offset: Offset(2,2),
          ),],
          color: Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              data.title,
              style: AppTypography.copyWith(
                weight: FontWeight.w700,
                size: 16,
              ),
            ),
            const SizedBox(height: 8),
            Text(data.director),
            const SizedBox(height: 24),
            Wrap(
              direction: Axis.horizontal,
              spacing: 12,
              runSpacing: 12,
              children: data.genres.map((e) {
                final genre = GenreItem(
                  item: e,
                  selected: const [],
                  onTap: () {},
                );

                return genre;
              }).toList(),
            ),
          ],
        ),
      ),
    );
  }
}
