// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_list_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$MovieListStore on MovieListBase, Store {
  late final _$listAtom = Atom(name: 'MovieListBase.list', context: context);

  @override
  List<MovieEntity> get list {
    _$listAtom.reportRead();
    return super.list;
  }

  @override
  set list(List<MovieEntity> value) {
    _$listAtom.reportWrite(value, super.list, () {
      super.list = value;
    });
  }

  late final _$MovieListBaseActionController =
      ActionController(name: 'MovieListBase', context: context);

  @override
  void getMovies() {
    final _$actionInfo = _$MovieListBaseActionController.startAction(
        name: 'MovieListBase.getMovies');
    try {
      return super.getMovies();
    } finally {
      _$MovieListBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void searchMovies(String str) {
    final _$actionInfo = _$MovieListBaseActionController.startAction(
        name: 'MovieListBase.searchMovies');
    try {
      return super.searchMovies(str);
    } finally {
      _$MovieListBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
list: ${list}
    ''';
  }
}
