import 'package:mobx/mobx.dart';
import 'package:movie_collection/core/util/use_case.dart';
import 'package:movie_collection/domain/movies/domain/entity/movie_entity.dart';
import 'package:movie_collection/domain/movies/domain/use_cases/get_movie_use_case.dart';

part 'movie_list_store.g.dart';

class MovieListStore = MovieListBase with _$MovieListStore;

abstract class MovieListBase with Store {
  final GetMovieUseCase getMovieUseCase;

  MovieListBase(this.getMovieUseCase);

  @observable
  List<MovieEntity> list = [];

  @action
  void getMovies() {
    list = getMovieUseCase(NoParams());
  }

  @action
  void searchMovies(String str) {
    String search = str.toLowerCase();
    list = list.where((movie) => movie.title.toLowerCase().contains(search))
        .toList();
  }
}