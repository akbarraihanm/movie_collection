abstract class UseCase<T, R> {
  T call(R param);
}

class NoParams {}