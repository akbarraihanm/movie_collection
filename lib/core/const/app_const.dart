import 'package:movie_collection/domain/movies/data/models/movie_data.dart';

class AppConst {
  static List<String> movieGenres = [
    "Action", "Drama", "Sci-Fi", "Horror", "Animation"
  ];
  static List<MovieData> movies = [];
  static String noMovies = "No movie item here. Please add a movie into your collection";
}