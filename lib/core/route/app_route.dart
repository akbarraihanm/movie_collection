import 'package:auto_route/auto_route.dart';
import 'package:movie_collection/core/route/app_route.gr.dart';

@AutoRouterConfig()
class AppRoute extends $AppRoute {
  @override
  List<AutoRoute> get routes => [
    AutoRoute(page: SplashRoute.page, initial: true, path: '/splash'),
    AutoRoute(page: MovieListRoute.page, path: '/movies'),
    AutoRoute(page: MovieDetailRoute.page, path: '/movies/:id'),
  ];

}