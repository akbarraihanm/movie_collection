import 'package:get_it/get_it.dart';
import 'package:movie_collection/domain/movies/di/app_locator.dart';

final sl = GetIt.instance;

class AppLocator {
  AppLocator() {
    MovieDomainLocator();
  }
}